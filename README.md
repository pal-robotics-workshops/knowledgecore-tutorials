# [PAL training series] KnowledgeCore: introduction and tutorial



## Training presentation

[Access the training presentation](https://docs.google.com/presentation/d/1HGq8hJtgp94Bc9Qa5D9yUGiUL3H_lplNXNj_OfcPAbU/edit?usp=sharing)


## Tutorial

### Configure the Docker image

First, start the PAL Developer Docker image:

```
> xhost +
> docker run -it --net host --privileged --env DISPLAY --name ari-kb registry.gitlab.com/pal-robotics/ari-1922/dockers/pal-gallium-pal-metapkg-development-ari-41-dev:latest bash
```

(adjust the URL of the Docker image to your own PAL Developer Docker image)

Then, install the dependencies required for the tutorial:

```
> sudo apt update
> sudo apt install pal-gallium-knowledge-core pal-gallium-oro 
> sudo apt install pal-gallium-kb-rest
```

### Start KnowledgeCore

```
> source /opt/pal/gallium/setup.bash
> roslaunch knowledge_core knowledge_core.launch
```

In another terminal:

```
> docker exec -it ari-kb bash
> source /opt/pal/gallium/setup.bash
> rosrun kb_rest server.sh
```

Then, open [http://localhost:8001/](http://localhost:8001/) in a webbrowser.

### Interact with KnowledgeCore via a Python notebook


In a new terminal:

```
> docker exec -it ari-kb bash
> sudo apt install python3-notebook ipython3
> ipython3 notebook --allow-root
```

Then, open [http://localhost:8888/](http://localhost:8888/) in a webbrowser and create a new notebook.

Refer to the presentation slides for the exercises, and check [the notebook in this repository](KnowledgeCore_tutorial.ipynb) if you are stuck!